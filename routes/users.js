'use strict';

const router = require('koa-router')();
const usersController = require('../controllers/users');
router.prefix('/users');

router.get('/', usersController.list);
router.get('/:id', usersController.getById);
router.post('/', usersController.create);
router.put('/:id', usersController.updateById);
router.delete('/:id', usersController.removeById);

module.exports = router;

'use strict';

const router = require('koa-router')();
const projectsController = require('../controllers/projects');
router.prefix('/projects');

router.get('/', projectsController.list);
router.get('/:id', projectsController.getById);
router.post('/', projectsController.create);
router.put('/:id', projectsController.updateById);
router.delete('/:id', projectsController.removeById);

module.exports = router;

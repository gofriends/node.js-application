'use strict';

const router = require('koa-router')();
const tasksController = require('../controllers/tasks');
router.prefix('/tasks');

router.get('/', tasksController.list);
router.get('/:id', tasksController.getById);
router.post('/', tasksController.create);
router.put('/:id', tasksController.updateById);
router.delete('/:id', tasksController.removeById);

module.exports = router;

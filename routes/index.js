'use strict';

const router = require('koa-router')();
const users = require('./users');
const tasks = require('./tasks');
const projects = require('./projects');

router.prefix('/api');

router.use(users.routes(), users.allowedMethods());
router.use(tasks.routes(), tasks.allowedMethods());
router.use(projects.routes(), projects.allowedMethods());

module.exports = router;

'use strict';

exports.searchLike = function(value) {
  // Helper method for SQL LIKE emulation
  return { $regex: '.*' + value + '.*', $options: 'i' };
};

exports.searchByOwnStatus = function(query, queryObject) {
  if (queryObject.status) {
    query.status = { $in: queryObject.status };
  }
};

exports.searchByOwnMark = function(query, queryObject) {
  if (queryObject.marklte) {
    query.mark = query.mark || {};
    query.mark.$lte = queryObject.marklte;
  }

  if (queryObject.markgte) {
    query.mark = query.mark || {};
    query.mark.$gte = queryObject.markgte;
  }
};

exports.searchByCompletedTasksMark = function(query, queryObject) {
  const taskQuery = {};
  let checkTasks = false;

  if (queryObject.marklte) {
    checkTasks = true;
    taskQuery.status = ['completed'];
    taskQuery.$or = taskQuery.$or || [];
    taskQuery.$or.push({ mark: { $gt: queryObject.marklte }});
  }

  if (queryObject.markgte) {
    checkTasks = true;
    taskQuery.status = ['completed'];
    taskQuery.$or = taskQuery.$or || [];
    taskQuery.$or.push({ mark: { $lt: queryObject.markgte }});
  }

  return { taskQuery, checkTasks };
};

exports.searchByAuthor = function(query, queryObject) {
  const authorQuery = {};
  let checkAuthor = true;

  if (queryObject.authorname) {
    checkAuthor = true;
    authorQuery.$and = authorQuery.$and || [];

    const nameQuery = { $or: [] };
    nameQuery.$or.push({ 'name': exports.searchLike(queryObject.authorname) });
    nameQuery.$or.push({ 'i18n.name': exports.searchLike(queryObject.authorname) });

    authorQuery.$and.push(nameQuery);
  }

  if (queryObject.authorsurname) {
    checkAuthor = true;
    authorQuery.$and = authorQuery.$and || [];

    const surnameQuery = { $or: [] };
    surnameQuery.$or.push({ 'surname': exports.searchLike(queryObject.authorsurname) });
    surnameQuery.$or.push({ 'i18n.surname': exports.searchLike(queryObject.authorsurname) });

    authorQuery.$and.push(surnameQuery);
  }

  return { authorQuery, checkAuthor };
};

exports.searchByParticipants = function(query, queryObject) {
  const participantQuery = {};
  let checkParticipants = false;

  if (queryObject.participantname) {
    checkParticipants = true;
    participantQuery.$and = participantQuery.$and || [];

    const nameQuery = { $or: [] };

    if (typeof queryObject.participantname === 'string') {
      nameQuery.$or.push({ 'name': exports.searchLike(queryObject.participantname) });
      nameQuery.$or.push({ 'i18n.name': exports.searchLike(queryObject.participantname) });
    } else {
      nameQuery.$or.push({
        'name': {
          $or: queryObject.participantname
            .map((participantName) => exports.searchLike(participantName))
        }
      });
      nameQuery.$or.push({
        'i18n.name': {
          $or: queryObject.participantname
            .map((participantName) => exports.searchLike(participantName))
        }
      });
    }

    participantQuery.$and.push(nameQuery);
  }

  if (queryObject.participantsurname) {
    checkParticipants = true;
    participantQuery.$and = participantQuery.$and || [];

    const surnameQuery = { $or: [] };

    if (typeof queryObject.participantsurname === 'string') {
      surnameQuery.$or.push({ 'surname': searchHelper.searchLike(queryObject.participantsurname) });
      surnameQuery.$or.push({ 'i18n.surname': searchHelper.searchLike(queryObject.participantsurname) });
    } else {
      surnameQuery.$or.push({
        'surname': {
          $or: queryObject.participantsurname
            .map((participantSurname) => searchHelper.searchLike(participantSurname))
        }
      });
      surnameQuery.$or.push({
        'i18n.surname': {
          $or: queryObject.participantsurname
            .map((participantSurname) => searchHelper.searchLike(participantSurname))
        }
      });
    }

    participantQuery.$and.push(surnameQuery);
  }

  if (queryObject.participantid) {
    checkParticipants = true;
    participantQuery._id = {
      $in: queryObject.participantid
    };
  }

  return { participantQuery, checkParticipants };
};
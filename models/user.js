'use strict';

const modelFactory = require('./index');
const searchHelper = require('../helpers/search');

const options = {};

options.modelName = 'User';
options.schemaFields = {
  email: { type: String, unique: true, required: true },
  name: { type: String, required: true },
  surname: { type: String, required: true },
  i18n: {
    name: String,
    surname: String
  }
};
options.autoIncrement = true;

const model = modelFactory(options);

model.schema.path('email').validate(function (email) {
   var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
   return emailRegex.test(email);
}, 'Invalid user email');

model.schema.statics.constructSearchQuery = async function(queryObject) {
  const query = {};

  if (queryObject.name) {
    query.$and = query.$and || [];

    const nameQuery = { $or: [] };
    nameQuery.$or.push({ 'name': searchHelper.searchLike(queryObject.name) });
    nameQuery.$or.push({ 'i18n.name': searchHelper.searchLike(queryObject.name) });

    query.$and.push(nameQuery);
  }

  if (queryObject.surname) {
    query.$and = query.$and || [];

    const surnameQuery = { $or: [] };
    surnameQuery.$or.push({ 'surname': searchHelper.searchLike(queryObject.surname) });
    surnameQuery.$or.push({ 'i18n.surname': searchHelper.searchLike(queryObject.surname) });

    query.$and.push(surnameQuery);
  }

  return query;
};

module.exports = model.bake();

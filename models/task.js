'use strict';

const User = require('./user');
const modelFactory = require('./index');
const searchHelper = require('../helpers/search');

const options = {};

options.modelName = 'Task';
options.schemaFields = {
  name: { type: String, required: true },
  description: { type: String, required: true },
  mark: { type: Number, min: 0, default: 0 },
  status: {
    type: String,
    required: true,
    enum: [ 'active', 'inactive', 'declined', 'completed' ]
  },
  author: { type: Number, ref: 'User', required: true },
  participants: [{ type: Number, ref: 'User' }]
};
options.autoIncrement = true;
options.populate = [
  { path: 'author', select: 'id name surname' },
  { path: 'participants', select: 'id name surname' }
];

const model = modelFactory(options);

model.schema.statics.constructSearchQuery = async function(queryObject) {
  const query = {};

  if (queryObject.name) {
    query.name = searchHelper.searchLike(queryObject.name);
  }

  if (queryObject.description) {
    query.description = searchHelper.searchLike(queryObject.description);
  }

  searchHelper.searchByOwnMark(query, queryObject);
  searchHelper.searchByOwnStatus(query, queryObject);

  const { authorQuery, checkAuthor } = searchHelper.searchByAuthor(query, queryObject);
  const { participantQuery, checkParticipants } = searchHelper.searchByParticipants(query, queryObject);

  if (checkAuthor) {
    const matchedAuthors = await User.find(authorQuery);
    query.author = { $in: matchedAuthors.map((author) => author.id) };
  }

  if (checkParticipants) {
    const matchedParticipants = await User.find(participantQuery);
    query.participants = {};
    query.participants.$elemMatch = {
      $in: matchedParticipants.map((participant) => participant.id)
    };
  }

  return query;
};

module.exports = model.bake();

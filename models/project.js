'use strict';

const User = require('./user');
const Task = require('./task');
const modelFactory = require('./index');
const searchHelper = require('../helpers/search');

const options = {};

options.modelName = 'Project';
options.schemaFields = {
  name: { type: String, required: true },
  body: { type: String, required: true },
  status: {
    type: String,
    required: true,
    enum: [ 'active', 'inactive', 'declined', 'completed' ]
  },
  author: { type: Number, ref: 'User' },
  participants: [{ type: Number, ref: 'User' }],
  tasks: [{ type: Number, ref: 'Task' }]
};
options.schemaOptions = {
  toObject: { virtuals: true },
  toJSON: { virtuals: true }
};
options.autoIncrement = true;
options.populate = [
  { path: 'author', select: 'id name surname' },
  { path: 'participants', select: 'id name surname' },
  { path: 'tasks', select: 'id name status mark' }
];

const model = modelFactory(options);

model.schema.virtual('averageMark').get(function() {
  let result = 0;

  const completedTasks = this.tasks.filter((task) => {
    return task.status === 'completed';
  });

  if (completedTasks.length) {
    result = completedTasks.reduce((memo, task) => (memo + task.mark), 0)/completedTasks.length;
  }

  return result;
});

model.schema.statics.constructSearchQuery = async function(queryObject) {
  const query = {};

  if (queryObject.name) {
    query.name = searchHelper.searchLike(queryObject.name);
  }

  if (queryObject.body) {
    query.body = searchHelper.searchLike(queryObject.body);
  }

  searchHelper.searchByOwnStatus(query, queryObject);

  const { authorQuery, checkAuthor } = searchHelper.searchByAuthor(query, queryObject);
  const { participantQuery, checkParticipants } = searchHelper.searchByParticipants(query, queryObject);
  const { taskQuery, checkTasks } = searchHelper.searchByCompletedTasksMark(query, queryObject);

  if (checkAuthor) {
    const matchedAuthors = await User.find(authorQuery);
    query.author = { $in: matchedAuthors.map((author) => author.id) };
  }

  if (checkParticipants) {
    const matchedParticipants = await User.find(participantQuery);
    query.participants = {};
    query.participants.$elemMatch = {
      $in: matchedParticipants.map((participant) => participant.id)
    };
  }

  if (checkTasks) {
    const matchedTasks = await Task.find(taskQuery);
    query.tasks = { $not: { $in: matchedTasks.map((task) => task.id) } };
  }

  return query;
};

module.exports = model.bake();

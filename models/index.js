'use strict';

const Promise = require('bluebird');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

function populateQuery(query, populateArray) {
  populateArray.forEach((populateItem) => {
    query = query.populate(populateItem);
  });

  return query;
}

module.exports = function modelFactory(options) {
  const modelName = options.modelName;
  const schemaFields = options.schemaFields;
  const schemaOptions = options.schemaOptions;
  const populateArray = options.populate || [];
  const useAutoIncrement = options.autoIncrement;

  const model = {};

  if (!schemaFields) {
    throw new Error('Schema fields are not specified');
  }

  if (!modelName) {
    throw new Error('Model name is not specified');
  }

  model.schema = new mongoose.Schema(schemaFields, schemaOptions);
  model.name = modelName;
  model.bake = function() {
    return mongoose.model(model.name, model.schema);
  };

  if (useAutoIncrement) {
    model.schema.plugin(autoIncrement.plugin, model.name);
  }

  model.schema.statics.create = function(fields) {
    return new this(fields).save();
  };

  model.schema.statics.getById = function(id) {
    return populateQuery(this.findOne({ _id: id }), populateArray).exec();
  };

  model.schema.statics.updateById = function(id, fields) {
    return populateQuery(this.findOneAndUpdate(
      { _id: id },
      fields,
      { new: true }
    ), populateArray).exec();
  };

  model.schema.statics.removeById = function(id) {
    return this.findOneAndRemove({ _id: id }).exec();
  };

  model.schema.statics.constructSearchQuery = function(queryObject) {
    // Should be overriden
    return Promise.resolve(queryObject);
  };

  model.schema.statics.search = async function(queryObject, limit, pageNum) {
    const result = {};

    const query = await this.constructSearchQuery(queryObject);

    result.data = await this.findPage(query, limit, pageNum);
    result.pages = await this.countPages(query, limit);
    result.limit = limit;
    result.page = pageNum;

    return result;
  }

  model.schema.statics.findPage = function(query, limit, pageNum) {
    return Promise.resolve()
      .then(() => {
        if (typeof limit !== 'number' || limit < 1) {
          throw new Error('Invalid "limit" parameter');
        }
        if (typeof pageNum !== 'number' || pageNum < 1) {
          throw new Error('Invalid "pageNum" parameter');
        }

        return populateQuery(
          this.find(query)
          .limit(limit)
          .skip((pageNum - 1)*limit)
          , populateArray).exec();
      });
  };

  model.schema.statics.countPages = function(query, limit) {
    return Promise.resolve()
      .then(() => {
        if (typeof limit !== 'number' || limit < 1) {
          throw new Error('Invalid "limit" parameter');
        }

        return this.count(query).exec();
      })
      .then((count) => Math.ceil(count/limit));
  };

  return model;
};

'use strict';

module.exports = function controllerFactory(service) {
  const controller = {};

  controller.service = service;

  if (!controller.service) {
    throw new Error('Controller service is not specified');
  }

  controller.list = async function(ctx) {
    try {
      ctx.body = await service.search(ctx.query, ctx.query.limit, ctx.query.page);
    } catch (err) {
      ctx.status = 400;
      ctx.body = { error: err.message };
    }
  };

  controller.create = async function(ctx) {
    try {
      ctx.body = await service.create(ctx.request.body);
    } catch (err) {
      ctx.status = 400;
      ctx.body = { error: err.message };
    }
  };

  controller.getById = async function(ctx) {
    const entity = await service.getById(ctx.params.id);

    if (!entity) {
      ctx.status = 404;
      ctx.body = { error: 'Not Found' };
      return;
    }

    ctx.body = { data: entity };
  };

  controller.updateById = async function(ctx) {
    const entity = await service.updateById(ctx.params.id, ctx.request.body);

    if (!entity) {
      ctx.status = 404;
      ctx.body = { error: 'Not Found' };
      return;
    }

    ctx.body = { data: entity };
  };

  controller.removeById = async function(ctx) {
    const entity = await service.removeById(ctx.params.id);

    if (!entity) {
      ctx.status = 404;
      ctx.body = { error: 'Not Found' };
      return;
    }

    ctx.body = {};
  };

  return controller;
};

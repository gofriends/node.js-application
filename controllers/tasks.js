'use strict';

const controllerFactory = require('./index');
const taskService = require('../services/tasks');

module.exports = controllerFactory(taskService);

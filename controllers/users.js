'use strict';

const controllerFactory = require('./index');
const userService = require('../services/users');

module.exports = controllerFactory(userService);

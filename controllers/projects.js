'use strict';

const controllerFactory = require('./index');
const projectService = require('../services/projects');

module.exports = controllerFactory(projectService);

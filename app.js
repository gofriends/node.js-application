const Koa = require('koa');
const app = new Koa();
const json = require('koa-json');
const bodyparser = require('koa-bodyparser');
const logger = require('koa-logger');

const db = require('./db');

db.init('mongodb://127.0.0.1/test-tm');

const api = require('./routes');

// middlewares
app.use(bodyparser({
  enableTypes:['json']
}));
app.use(json());
app.use(logger());

// logger
app.use(async (ctx, next) => {
  const start = new Date();
  await next();
  const ms = new Date() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
});

// routes
app.use(api.routes(), api.allowedMethods());
app.use(async (ctx, next) => {
  if (ctx.status === 404) {
    return ctx.body = { error: 'Not Found' };
  }

  next();
});

// error-handling
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
  ctx.status = 500;
  ctx.body = { error: 'Internal Server Error' };
});

module.exports = app;

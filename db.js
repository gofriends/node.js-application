'use strict';

const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

exports.init = function(dbUrl) {
  const connection = mongoose.connect(dbUrl);
  autoIncrement.initialize(connection);

  mongoose.Promise = require('bluebird');
};

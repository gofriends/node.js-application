'use strict';

const serviceFactory = require('./index');
const User = require('../models/user');

module.exports = serviceFactory(User);

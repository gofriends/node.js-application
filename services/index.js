'use strict';

module.exports = function serviceFactory(model, options) {
  options = options || {};

  const service = {};

  service.model = model;
  service.searchLimit = options.searchLimit || 10;

  if (!service.model) {
    throw new Error('Service model is not specified');
  }

  service.search = function(queryObject, limit, pageNum) {
    limit = Number(limit || service.searchLimit);
    pageNum = Number(pageNum || 1);

    return model.search(queryObject, limit, pageNum);
  };

  service.create = function(fields) {
    return model.create(fields);
  };

  service.getById = function(id) {
    return model.getById(id);
  };

  service.updateById = function(id, fields) {
    return model.updateById(id, fields);
  };

  service.removeById = function(id) {
    return model.removeById(id);
  };

  return service;
};

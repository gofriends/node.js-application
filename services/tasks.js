'use strict';

const serviceFactory = require('./index');
const Task = require('../models/task');

module.exports = serviceFactory(Task);

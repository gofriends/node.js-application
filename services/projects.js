'use strict';

const serviceFactory = require('./index');
const Project = require('../models/project');

module.exports = serviceFactory(Project);
